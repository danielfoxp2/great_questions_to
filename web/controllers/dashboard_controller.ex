defmodule GreatQuestionsTo.DashboardController do
  use GreatQuestionsTo.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
