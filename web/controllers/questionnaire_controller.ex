defmodule GreatQuestionsTo.QuestionnaireController do
  use GreatQuestionsTo.Web, :controller

  def new(conn, params) do
    render conn, "new.html"
  end
end
