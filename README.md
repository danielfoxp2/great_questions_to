# Introduction #

This project is to help my wife on her research for her doc degree.
It will contain:

* Questions creator;

* Research runner;

* Answers exportations;

	* As csv;
	
	* As spreadsheet;
	
* Answers integration;

	* With SPSS;
	
	* And possible others in the future;
	
* Management of:

	* Team;
	
	* Projects;

### How do I get set up? ###

I am using docker as my development environment and heroku as my provider.

With docker I have set up or use from 3rd party several files to mount the environment, as:

* Database => kiasaki/alpine-postgres Dockerfile

	* `docker run --name db_name -e POSTGRES_USER=pg_user -e POSTGRES_PASSWORD=pg_pass -d kiasaki/alpine-postgres`
	
* Volume => tianon/true Dockerfile

	* `docker run --name source -v /work tianon/true`
	
* Language => danielfoxp2/Docker_Environments/elrx Dockerfile

	* `docker build --build-arg phoenix_version=phoenix_new -t phoenix_1_2 .`
	
	* `docker build --build-arg phoenix_version=phx_new -t phoenix_1_3 .`
	
	* `docker run --name elixir -dt -p $HOST_PORT:$CONTAINER_PORT --link db_name:db_name --volumes-from source phoenix_1_3`
	
* Editor => danielfoxp2/Docker_Environments/editor Dockerfile

	* `docker build --build-arg alternate_files=vim-openalternatefile-phx1.2 -t flex_editor .`
	
	* `docker build --build-arg alternate_files=vim-openalternatefile-phx1.3 -t flex_editor .`
	
	* `docker run --name editor -it --rm -v /var/run/docker.sock:/var/run/docker.sock --volumes-from source flex_editor bash`
	

In the future new docker files will be used to achieve a continuous delivery environment.

### Tips

* Tmux has a "problem" with utf8. So when use it in container is good to make an alias like below (the "-2" is not needed, but helps with vim colors):

    * `alias tm='tmux -u2'`

* In this gist `https://gist.github.com/danielfoxp2/d39efa83dbbfe88425c5c6be6b349a1f` is the code to alternate between js spec and js prod code. It works to phoenix 1.2. Maybe will be needed some changes to phoenix 1.3 (idk, yet).



# GoodQuestions (Phoenix automatic created this part)

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
