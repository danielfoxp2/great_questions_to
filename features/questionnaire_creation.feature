Feature: Questionnaire Creation
  As an academic researcher
  I want to create questionnaires 
  To be able to collect data to my research

  Scenario: Start the creation of a new questionnaire
    Given that I am on dashboard
    And I start the creation of a new questionnaire
    When I add a new question to the questionnaire
    Then I am asked what is the question description
    And I can select what type of questions is it


