defmodule GreatQuestionsTo.WallabyRunner do
  
  def set_all_needed_to_start() do
    Application.put_env(:wallaby, :base_url, GreatQuestionsTo.Endpoint.url)
    _ok = Ecto.Adapters.SQL.Sandbox.checkout(GreatQuestionsTo.Repo)

    Ecto.Adapters.SQL.Sandbox.mode(GreatQuestionsTo.Repo, {:shared, self()})
    metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(GreatQuestionsTo.Repo, self())
    {:ok, session} = Wallaby.start_session(metadata: metadata)

    %{session: session}
  end

end
