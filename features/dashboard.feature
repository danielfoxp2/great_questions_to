Feature: Dashboard 
  As an academic researcher
  I want to see a dashboard with all stuff that I can do
  To be able to choose something to work with

  Scenario: Start a new research interview
    When I log in
    Then I should see the possibility to start a new research interview

  Scenario: See questionnaires applied on research
    When I log in
    Then I should be able to check all applied questionnaires of research

  Scenario: Being able to export questionnaires
    When I log in
    Then I should be able to export applied questionnaires

  Scenario: See the possibility to add a new questionnaire
    When I log in
    Then I should see the possibility to make a new questionnaire

  Scenario: See the possibility to show created questionnaires of the research
    When I log in
    Then I should see the option to show all questionnaires of the research

  Scenario: Add a new researcher
    When I log in
    Then I should see the option to add a new researcher

  Scenario: See all researchers
    When I log in
    Then I should be able to see all registered researchers

  Scenario: Log out
    When I log in
    Then I should be able to log out

  Scenario: Check logged user profile
    When I log in
    Then I am able to see my user profile
