defmodule GreatQuestionsTo.DashboardContext do
  use WhiteBread.Context
  use Wallaby.DSL
  import Wallaby.Query

  when_ ~r/^I log in$/, fn %{session: session} = state ->
    session
    |> visit("/dashboard")

    {:ok, state}
  end

  then_ ~r/^I should see the possibility to make a new questionnaire$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#add-questionnaire", text: "Questionário"))

    {:ok, state}
  end

  then_ ~r/^I should see the option to show all questionnaires of the research$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#show-questionnaires", text: "Existentes"))

    {:ok, state}
  end

  then_ ~r/^I should see the possibility to start a new research interview$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#new_interview", text: "Entrevista"))
    
    {:ok, state}
  end

  then_ ~r/^I should be able to check all applied questionnaires of research$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#see_all_applied", text: "Questionários realizados"))

    {:ok, state}
  end  

  then_ ~r/^I should be able to export applied questionnaires$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#export_questionnaires", text: "Exportar questionários"))

    {:ok, state}
  end    

  then_ ~r/^I should be able to see all registered researchers$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#researchers", text: "Ver todos"))

    {:ok, state}
  end

  then_ ~r/^I should see the option to add a new researcher$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#add-researcher", text: "Pesquisador"))

    {:ok, state}
  end

  then_ ~r/^I should be able to log out$/, fn %{session: session} = state ->
    # The "visible: false" is a hack because I was not able to 
    # make wallaby identify those anchors without it
    session
    |> assert_has(css("#log_out", visible: false))

    {:ok, state}
  end

  then_ ~r/^I am able to see my user profile$/, fn %{session: session} = state ->
    session
    |> assert_has(css("#see_profile", visible: false))

    {:ok, state}
  end

end
