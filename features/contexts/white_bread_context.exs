defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Wallaby.DSL

  Code.require_file("features/support/wallaby_runner.exs")
  alias GreatQuestionsTo.WallabyRunner
  
  scenario_starting_state fn _state ->
    WallabyRunner.set_all_needed_to_start()
  end

  import_steps_from GreatQuestionsTo.DashboardContext
  import_steps_from GreatQuestionsTo.QuestionnaireCreationContext

end
