defmodule GreatQuestionsTo.QuestionnaireCreationContext do
  use WhiteBread.Context
  use Wallaby.DSL
  import Wallaby.Query

  given_ ~r/^that I am on dashboard$/, fn state ->
      {:ok, state}
  end

  when_ ~r/^I start the creation of a new questionnaire$/, fn state ->
      {:ok, state}
  end

  then_ ~r/^I should see something$/, fn state ->
      {:ok, state}
  end
  
end
