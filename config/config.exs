# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :great_questions_to,
  ecto_repos: [GreatQuestionsTo.Repo]

# Configures the endpoint
config :great_questions_to, GreatQuestionsTo.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Qv6Ok4eNtb32AttcuwXk87/t5OHdvifkhWDAanfq+FyyV4RZ398iCUWxpp3x8mDy",
  render_errors: [view: GreatQuestionsTo.ErrorView, accepts: ~w(html json)],
  pubsub: [name: GreatQuestionsTo.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
