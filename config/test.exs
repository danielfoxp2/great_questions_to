use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :great_questions_to, GreatQuestionsTo.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

config :great_questions_to, :sql_sandbox, true

# Configure your database
config :great_questions_to, GreatQuestionsTo.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("UFG_PG_ENV_POSTGRES_USER"),
  password: System.get_env("UFG_PG_ENV_POSTGRES_PASSWORD"),
  database: "great_questions_to_test",
  hostname: System.get_env("UFG_PG_PORT_5432_TCP_ADDR"),
  pool: Ecto.Adapters.SQL.Sandbox
